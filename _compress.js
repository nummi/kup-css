const minify = require('@node-minify/core');
const cssnano = require('@node-minify/cssnano');

minify({
  compressor: cssnano,
  input: '_temp.css',
  output: 'utils.css'
});
